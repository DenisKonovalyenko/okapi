package net.sf.okapi.xliffcompare.integration;

import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.common.integration.XliffCompareIT;
import net.sf.okapi.filters.xml.XMLFilter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

@RunWith(JUnit4.class)
public class DocBookXliffCompareIT extends XliffCompareIT {
	private static final String CONFIG_ID = "okf_xml-docbook";
	private static final String DIR_NAME = "/docbook/";
	private static final List<String> EXTENSIONS = Arrays.asList(".xml");

	public DocBookXliffCompareIT() {
		super(CONFIG_ID, DIR_NAME, EXTENSIONS, XMLFilter::new);
	}

	@Test
	public void docBookXliffCompareFiles() throws FileNotFoundException, URISyntaxException {
		realTestFiles(false, new FileComparator.XmlComparator());
	}
}
