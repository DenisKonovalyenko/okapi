package net.sf.okapi.roundtrip.integration;

import net.sf.okapi.common.integration.EventRoundTripIT;
import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.filters.properties.PropertiesFilter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

@RunWith(JUnit4.class)
public class RoundTripPropertyIT extends EventRoundTripIT {
	private static final String CONFIG_ID = "okf_properties";
	private static final String DIR_NAME = "/property/";
	private static final List<String> EXTENSIONS = Arrays.asList(".properties");

	public RoundTripPropertyIT() {
		super(CONFIG_ID, DIR_NAME, EXTENSIONS, PropertiesFilter::new);
	}

	@Test
	public void propertiesFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(false);
		realTestFiles(false, new FileComparator.EventComparator());
	}

	@Test
	public void propertiesSerializedFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(true);
		realTestFiles(CONFIG_ID, false, new FileComparator.EventComparatorWithWhitespace());
	}
}
