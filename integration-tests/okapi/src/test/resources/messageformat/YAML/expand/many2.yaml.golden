# Simple Placeholder
- Hello, {name}!

# Select Placeholder
- |
  {gender, select,
    female {She is a doctor.}
    male {He is a doctor.}
    other {They are doctors.}}

# Date/Time Formatting
- |
  Today is {date, date,  long} and the time is {time, time,  short}

# Nested Placeholders
- |
  {gender, select,
    female {Welcome, Ms. {name}!}
    male {Welcome, Mr. {name}!}
    other {Welcome, {name}!}}

# Custom Units
- |
  {unit, select,
    km {You have walked {distance, number} kilometers.}
    m {You have walked {distance, number} meters.}
    mi {You have walked {distance, number} miles.}
    other {You have walked {distance, number} units.}}

# Ordinal Numbers
- |
  {place, selectordinal,
    other {You finished #th in the race.}}

# Complex Placeholder
- |
  {status, select,
    success {{name} completed successfully.}
    error {{name} encountered an error.}
    in_progress {{name} is in progress.}
    other {Status unknown for {name}.}}

# Selectordinal Placeholder
- |
  {rank, selectordinal,
    other {You are the #th person in line.}}


# Currency Formatting
- |
  Your total amount is {amount, number,  currency}.

# Choice Placeholder
- |
  {age, select,
    0 {Newborn}
    1 {Infant}
    2 {Toddler}
    3 {Preschooler}
    4 {Child}
    5 {Kid}
    6 {Preteen}
    7 {Teenager}
    16 {Young adult}
    18 {Adult}
    other {Senior}}

# Date and Time Formatting
- |
  Today's date is {current_date, date,  long}.

- |
  The time is {current_time, time,  short}.

# Ordinal Numbers
- |
  {level, selectordinal,
    other {You have reached the #th level.}}

# Plural with Choices
- |
  {count, plural,
    one {You have one unread message.}
    other {You have {count} unread messages.}
    zero {You have {count} unread messages.}
    two {You have {count} unread messages.}
    few {You have {count} unread messages.}
    many {You have {count} unread messages.}}

# Gender-specific Greeting
- |
  {gender, select,
    female {Hello, Ms. {name}!}
    male {Hello, Mr. {name}!}
    other {Hello, {name}!}}

# Currency Formatting
- |
  Your total balance is {amount, number,  currency}.

# Select with Ranges
- |
  {age, select,
    0 {You have no children.}
    1 {You have one child.}
    2 {You are an adult.}
    3 {You are an elderly person.}
    other {You are in an undefined stage of life.}}

# Choice with Nested Select
- |
  {gender, select,
    female {{role, select,
        athlete {She is an athlete.}
        scientist {She is a scientist.}
        other {She has an undefined role.}}}
    male {{role, select,
        athlete {He is an athlete.}
        scientist {He is a scientist.}
        other {He has an undefined role.}}}
    other {{role, select,
        athlete {They are an athlete.}
        scientist {They are a scientist.}
        other {They have an undefined role.}}}}

# Ordinal Plural
- |
  {rank, selectordinal,
    other {You are the #th place winner.}}

# Date and Time Formatting
- |
  Today is {current_date, date,  full}.

- |
  The time is {current_time, time,  medium}.

# Nested Select with Plural
- |
  {gender, select,
    female {{num_children, plural,
        =0 {She has no children.}
        one {She has one child.}
        other {She has # children.}
        zero {She has # children.}
        two {She has # children.}
        few {She has # children.}
        many {She has # children.}}}
    male {{num_children, plural,
        =0 {He has no children.}
        one {He has one child.}
        other {He has # children.}
        zero {He has # children.}
        two {He has # children.}
        few {He has # children.}
        many {He has # children.}}}
    other {{num_children, plural,
        =0 {They have no children.}
        one {They have one child.}
        other {They have # children.}
        zero {They have # children.}
        two {They have # children.}
        few {They have # children.}
        many {They have # children.}}}}
