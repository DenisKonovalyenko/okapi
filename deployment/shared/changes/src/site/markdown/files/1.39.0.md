# Changes from M38 to 1.39.0

<!-- MACRO{toc} -->

## Filters

* HTML Filter

    * Fixed a bug that could cause some conditional rule matches to fail to apply.

* IDML Filter

    * [Issue #897](https://bitbucket.org/okapiframework/okapi/issues/897): Add an option to expose line breaks as inline codes rather than separate text units.

* Markdown Filter

    * [Issue #886](https://bitbucket.org/okapiframework/okapi/issues/886), [#888](https://bitbucket.org/okapiframework/okapi/issues/888): Expose formatting tags as paired codes, rather than standalone

* MIF Filter

    * [Issue #901](https://bitbucket.org/okapiframework/okapi/issues/901): Remove support for FrameMaker 7 and older
    * [Issue #893](https://bitbucket.org/okapiframework/okapi/issues/893): Fix a bug that caused some hidden pages to be exposed for translation even when the filter was configured not to extract hidden pages.
    * [Issue #895](https://bitbucket.org/okapiframework/okapi/issues/895): Support multiple text frames per page
    * [Issue #902](https://bitbucket.org/okapiframework/okapi/issues/902), [#886](https://bitbucket.org/okapiframework/okapi/issues/904): Correctly extract content from anchored frames, including nested frames
    * [Issue #896](https://bitbucket.org/okapiframework/okapi/issues/896), [#904](https://bitbucket.org/okapiframework/okapi/issues/904): Improve extraction of paragraph number formatting declarations. These will no be extracted as separate text units that correspond to the formatting template.
    * [Issue #896](https://bitbucket.org/okapiframework/okapi/issues/896): Streamline inline codes in some cases.

* OpenXML Filter

    * [Issue #847](https://bitbucket.org/okapiframework/okapi/issues/847): Fix a bug when removing revision markers related to paragraph deletion
    * [Issue #878](https://bitbucket.org/okapiframework/okapi/issues/878): Fix a bug where the filter would incorrectly strip the first defined style in documents that did not have default styles defined.
    * [Issue #882](https://bitbucket.org/okapiframework/okapi/issues/882): Fix a crash
    * [Issue #851](https://bitbucket.org/okapiframework/okapi/issues/851), [#888](https://bitbucket.org/okapiframework/okapi/issues/888): Improve handling of fonts
    * [Issue #884](https://bitbucket.org/okapiframework/okapi/issues/884), [#887](https://bitbucket.org/okapiframework/okapi/issues/887): Streamline tag extraction, particularly in text units containing only a single formatting run.
    * [Issue #899](https://bitbucket.org/okapiframework/okapi/issues/899): Improve handling of bidirectional text

* Regex Filter

    * Add new regular expression based rules `metaRules` that create named metadata entries from notes

* Versified Text Filter

    * **This filter has been removed.**

* XLIFF (1.2) Filter

    * [Issue #855](https://bitbucket.org/okapiframework/okapi/issues/855): Support the `context-group` element on import and export.  When writing XLIFF, encode non-standard metadata as `context-group/context`, rather than using the proprietary `okp:meta` scheme.
    * [Issue #875](https://bitbucket.org/okapiframework/okapi/issues/875): Improve handling of XLIFF documents containing WorldServer-specific (`iws:`) metadata.

* XMLStream Filter

    * Fixed a bug that could cause some conditional rule matches to fail to apply.

## Connectors

* Google MT

    * Added a new option `failuresBeforeAbort` which sets the number of failed queries (after retries if any is allowed) after which the connector throws an exception to fail.

* Google AutoML Translation

    * The connector now uses the production endpoints (`https://automl.googleapis.com/v1` instead of `https://automl.googleapis.com/v1beta1`).

## Libraries

* Verification

    * Added a new option `useGenericCodes` to the Checker parameters to use the generic
      inline codes representation in the display text provided with the issues.
      This also affect the start/end positions of the highlighted text (if there is any).
    * Fixed [issue #910](https://bitbucket.org/okapiframework/okapi/issues/910):
      Pointing to the wrong code when one of two codes with the same original data is missing.

## Steps

* XLIFF Splitter

    * Add an option to restore file names based on the value of `file/@original`
