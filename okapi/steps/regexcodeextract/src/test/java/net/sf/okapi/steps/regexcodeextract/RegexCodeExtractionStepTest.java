/*===========================================================================
  Copyright (C) 2023 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.steps.regexcodeextract;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.EventType;
import net.sf.okapi.common.resource.Code;
import net.sf.okapi.common.resource.ISegments;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.StartDocument;
import net.sf.okapi.common.resource.TextFragment;
import net.sf.okapi.common.resource.TextUnit;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(JUnit4.class)
public class RegexCodeExtractionStepTest {

  @Test
  public void testGetDescription() {
    RegexCodeExtractionStep step = new RegexCodeExtractionStep();
    assertNotNull("Step message is not null", step.getDescription());
    assertTrue("Step message is not zero length", step.getDescription().length() >= 1);
  }

  @Test
  public void testParametersClassBasic() {
    RegexCodeExtractionStep step = new RegexCodeExtractionStep();
    RegexCodeExtractionStepParameters params = step.getParameters();
    params.addCodeFinderRule("\\{\\{.*?}}");
    params.addCodeFinderRule("</?[a-zA-Z0-9]+?>");
    assertEquals(2, params.getCodeFinderRules().size());
    assertTrue("rule1 must exist.", params.toString().contains("rule1="));
  }

  @Test
  public void testParametersClassSaveRestore() {
    RegexCodeExtractionStep step = new RegexCodeExtractionStep();
    RegexCodeExtractionStepParameters params = step.getParameters();
    params.addCodeFinderRule("ruleA");
    params.addCodeFinderRule("ruleB");
    String savedConf = params.toString();
    params.reset();
    assertEquals(0, params.getCodeFinderRules().size());
    params.addCodeFinderRule("ruleC");
    assertEquals(1, params.getCodeFinderRules().size());
    params.fromString(savedConf);
    assertEquals(2, params.getCodeFinderRules().size());
    assertTrue("ruleB must exist.", params.toString().contains("ruleB"));
  }

  @Test
  public void testParametersClassAddRules() {
    RegexCodeExtractionStep step = new RegexCodeExtractionStep();
    RegexCodeExtractionStepParameters params = step.getParameters();
    params.addCodeFinderRules(Arrays.asList("ruleA", "ruleB", "ruleC"));
    assertEquals(3, params.getCodeFinderRules().size());
  }

  @Test
  public void testSimpleOneMustacheLikeRule() { // Test the step with just one regex.
    RegexCodeExtractionStep step = new RegexCodeExtractionStep();
    RegexCodeExtractionStepParameters params = step.getParameters();
    params.setCodeFinderRules("#v1\ncount.i=1\nrule0=\\{\\{.*?}}\n");

    StartDocument sd = new StartDocument("dummyDoc");
    sd.setMimeType("text/plain");
    sd.setEncoding(StandardCharsets.UTF_16BE.toString(), false);
    sd.setLineBreak("\n");
    step.handleEvent(new Event(EventType.START_DOCUMENT, sd));

    ITextUnit tu = new TextUnit("1", "Hello, {{name}}, your email address is {{email}} and phone number is {{phone}}.");
    Event e = step.handleEvent(new Event(EventType.TEXT_UNIT, tu));

    assertTrue(e.isTextUnit());
    tu = e.getTextUnit();
    ISegments segs = tu.getSourceSegments();
    assertEquals(1, segs.count());
    TextFragment tf = segs.getFirstContent();
    assertEquals(3, tf.getCodes().size());
    Code c1 = tf.getCode(1);
    assertEquals("{{email}}", c1.getData());
  }

  @Test
  public void testTwoRules() { // One regex to handle mustache-like tags and another to handle HTML like tag.
    RegexCodeExtractionStep step = new RegexCodeExtractionStep();
    RegexCodeExtractionStepParameters params = step.getParameters();
    params.addCodeFinderRule("\\{\\{.*?}}");
    params.addCodeFinderRule("</?[a-zA-Z0-9]+?>");

    StartDocument sd = new StartDocument("dummyDoc");
    sd.setMimeType("text/plain"); // For this test, we pretend this were a plain text file and let the step parse it.
    sd.setEncoding(StandardCharsets.UTF_16BE.toString(), false);
    sd.setLineBreak("\n");
    step.handleEvent(new Event(EventType.START_DOCUMENT, sd));

    ITextUnit tu = new TextUnit("1", "Hello, <b>{{name}}</b>, your email address is <i>{{email}}</i> and phone number is <strong>{{phone}}</strong>.");
    Event e = step.handleEvent(new Event(EventType.TEXT_UNIT, tu));

    tu = e.getTextUnit();
    ISegments segs = tu.getSourceSegments();
    assertEquals(1, segs.count());
    TextFragment tf = segs.getFirstContent();
    assertEquals(9, tf.getCodes().size());
    Code c4 = tf.getCode(4);
    assertEquals("{{email}}", c4.getData());
    Code c8 = tf.getCode(8);
    assertEquals("</strong>", c8.getData());
  }
}
