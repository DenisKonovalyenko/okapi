/*
 * =============================================================================
 * Copyright (C) 2010-2022 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.steps.xliffsplitter;

import net.sf.okapi.common.BOMAwareInputStream;
import org.slf4j.Logger;
import org.xmlunit.builder.DiffBuilder;
import org.xmlunit.builder.Input;
import org.xmlunit.diff.Diff;
import org.xmlunit.diff.Difference;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;

import static org.junit.Assert.fail;

final class XmlDocumentsComparison {
    private final Path inputRoot;
    private final Path outputRoot;
    private final Logger logger;

    XmlDocumentsComparison(final Path inputRoot, final Path outputRoot, final Logger logger) {
        this.inputRoot = inputRoot;
        this.outputRoot = outputRoot;
        this.logger = logger;
    }

    void compareWithGold(String directory, String outputFileName, String goldFileName) throws Exception {
        File gold = inputRoot.resolve(goldFileName).toFile();
        File out = outputRoot.resolve(directory).resolve(outputFileName).toFile();
        BOMAwareInputStream goldS = new BOMAwareInputStream(new FileInputStream(gold), "UTF-8");
        goldS.detectEncoding();
        BOMAwareInputStream outS = new BOMAwareInputStream(new FileInputStream(out), "UTF-8");
        outS.detectEncoding();
        try (Reader goldR = new InputStreamReader(goldS, StandardCharsets.UTF_8);
             Reader outR = new InputStreamReader(outS, StandardCharsets.UTF_8)) {
            compareXML(goldR, outR, gold.getAbsolutePath(), out.getAbsolutePath());
        }
    }

    private void compareXML(Reader goldR, Reader outR, String goldName, String outName) throws Exception {
        final Diff diff = DiffBuilder.compare(Input.fromReader(goldR))
            .withTest(Input.fromReader(outR))
            .checkForIdentical()
            .build();
        if (diff.hasDifferences()) {
            logger.warn("Differences between {} and {}:", goldName, outName);
            for (Difference d : diff.getDifferences()) {
                logger.warn("- {}", d.toString());
            }
            fail();
        }
    }
}
