/*===========================================================================
   Copyright (C) 2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.steps.cleanup;

import net.sf.okapi.common.IResource;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.filterwriter.GenericContent;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.Segment;
import net.sf.okapi.common.resource.TextContainer;
import net.sf.okapi.common.resource.TextFragment;
import net.sf.okapi.common.resource.TextPart;
import net.sf.okapi.common.resource.TextUnit;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(JUnit4.class)
public class CleanerUnitTest {

	private final LocaleId locFR = LocaleId.FRENCH;

	private GenericContent fmt;

	@Before
	public void setup() {

		fmt = new GenericContent();
	}
	
	@Test @Ignore
	public void testCleanUnit() {

		// “t1, ” t2 ,“‘t3 , ’ t4 :”
		TextFragment srcTf1 = new TextFragment("\u201Ct1\u002C \u201D t2 \u002C\u201C\u2018t3 \u002C\u2019 t4 \u003A\u201D");
		// t1 “t2” ; . 352
		TextFragment srcTf2 = new TextFragment("t1 \u201Ct2\u201D \u003B \u002E 352");
		// t1 ‘t2 ’ ““t3 ! ””
		TextFragment srcTf3 = new TextFragment("t1 \u2018t2 \u2019 \u201C\u201Ct3 \u0021 \u201D\u201D");

		// « t1 » ,l’t2,« t3 , t4 » :
		TextFragment frTf1 = new TextFragment("\u00AB\u00A0t1\u00A0\u00BB \u002Cl\u2019t2\u002C\u00AB\u00A0t3 \u002C t4\u00A0\u00BB \u003A");
		// t1 «  t2» ;0, 352
		TextFragment frTf2 = new TextFragment("t1 \u00AB\u00A0 t2\u00BB\u003B0\u002C 352");
		// t1 ‘t2’ « t3 ! »
		TextFragment frTf3 = new TextFragment("t1 \u2018t2\u2019 \u00AB\u00A0t3\u00A0\u0021\u00A0\u00BB");

		ITextUnit tu = new TextUnit("tu1");
		TextContainer srcTc = tu.getSource();
		srcTc.append(new Segment("seg1", srcTf1));
		srcTc.append(new TextPart(" "));
		srcTc.append(new Segment("seg2", srcTf2));
		srcTc.append(new TextPart(" "));
		srcTc.append(new Segment("seg3", srcTf3));

		TextContainer trgTc = tu.createTarget(locFR, true, IResource.CREATE_EMPTY);
		trgTc.append(new Segment("seg1", frTf1));
		trgTc.append(new TextPart(" "));
		trgTc.append(new Segment("seg2", frTf2));
		trgTc.append(new TextPart(" "));
		trgTc.append(new Segment("seg3", frTf3));

		Cleaner cleaner = new Cleaner();
		for (Segment srcSeg : tu.getSourceSegments()) {
			Segment trgSeg = tu.getTargetSegment(locFR, srcSeg.getId(), false);
			assertNotNull(trgSeg);
			cleaner.normalizeQuotation(tu, srcSeg, locFR);
			cleaner.normalizePunctuation(srcSeg.text, trgSeg.text);
		}

		assertEquals("[\"t1,\" t2, \"\'t3,\' t4:\"]", fmt.printSegmentedContent(tu.getSource(), true, false));
		assertEquals("[\"t1,\" t2, \"\'t3,\' t4:\"] [t1 \"t2\"; .352] [t1 \'t2 \' \"\"t3!\"\"]", fmt.printSegmentedContent(tu.getSource(), true, false));
		assertEquals("[\"t1,\" t2, \"\'t3,\' t4:\"] [t1 \"t2\"; .352] [t1 \'t2 \' \"\"t3!\"\"]", fmt.printSegmentedContent(tu.getSource(), true, true));
		assertEquals("[\"t1\", l\'t2, \"t3, t4\":] [t1 \"t2\"; 0,352] [t1 \'t2\' \"t3!\"]", fmt.printSegmentedContent(tu.getTarget(locFR), true, false));
		assertEquals("[\"t1\", l\'t2, \"t3, t4\":] [t1 \"t2\"; 0,352] [t1 \'t2\' \"t3!\"]", fmt.printSegmentedContent(tu.getTarget(locFR), true, true));
	}

	@Test
	public void testWhiteSpaces() {
		TextFragment srcTf1 = new TextFragment("Hello,      World!   Too\t\tmany    spaces   here!");
		TextFragment trgTf1 = new TextFragment("¡Hola,     Mundo!    ¡Demaciados    espacios     aquí!");
		ITextUnit tu = new TextUnit("tu1");

		TextContainer srcTc = tu.getSource();
		srcTc.append(new Segment("seg1", srcTf1));

		TextContainer trgTc = tu.createTarget(LocaleId.SPANISH, true, IResource.CREATE_EMPTY);
		trgTc.append(new Segment("seg1", trgTf1));

		new Cleaner().run(tu, LocaleId.SPANISH);

		assertEquals("[Hello, World! Too many spaces here!]", fmt.printSegmentedContent(srcTc, true, false));
		assertEquals("[¡Hola, Mundo! ¡Demaciados espacios aquí!]", fmt.printSegmentedContent(trgTc, true, false));
	}

	@Test
	public void testPunctuations() {
		ITextUnit tu = new TextUnit("tu1");

		TextContainer srcTc = tu.getSource();
		srcTc.append(new Segment("seg1", new TextFragment("There should not be a space before   ; like this.")));
		srcTc.append(new TextPart(" "));
		srcTc.append(new Segment("seg2", new TextFragment("There should not be a space before   , either.")));

		TextContainer trgTc = tu.createTarget(LocaleId.SPANISH, true, IResource.CREATE_EMPTY);
		trgTc.append(new Segment("seg1", new TextFragment("No debería haber un espacio antes de  ;  como este.")));
		trgTc.append(new TextPart(" "));
		trgTc.append(new Segment("seg2", new TextFragment("Debe haber un espacio antes de ,  tampoco.")));

		Parameters params = new Parameters();
		params.setNormalizePunctuations(true);
//		params.setKeepCrNlIntact(true); // Uncomment this to verify keepCrNlIntact setting doesn't affect this test case.
		new Cleaner(params).run(tu, LocaleId.SPANISH);

		assertEquals("[There should not be a space before; like this.] [There should not be a space before, either.]", fmt.printSegmentedContent(srcTc, true, false));
		assertEquals("[No debería haber un espacio antes de; como este.] [Debe haber un espacio antes de, tampoco.]", fmt.printSegmentedContent(trgTc, true, false));
	}

	@Test
	public void testKeepCrNlIntactSimple() {
		ITextUnit tu = new TextUnit("tu1");

		TextContainer srcTc = tu.getSource();
		// Translation of the famous haiku by Matsuo Basho found in https://en.wikipedia.org/wiki/Haiku
		srcTc.append(new Segment("seg1", new TextFragment(
										"old pond\t\n" +
										"frog leaps in   \n" +
										"water's   sound")));

		TextContainer trgTc = tu.createTarget(LocaleId.JAPANESE, true, IResource.CREATE_EMPTY);
		trgTc.append(new Segment("seg1", new TextFragment(
										"古池や \n" +
										"蛙飛びこむ  \n" +
										"水の音")));

		Parameters params = new Parameters();
		params.setKeepCrNlIntact(true);
		new Cleaner(params).run(tu, LocaleId.JAPANESE);

		assertEquals("[old pond \nfrog leaps in \nwater's sound]", fmt.printSegmentedContent(srcTc, true, false));
		assertEquals("[古池や \n蛙飛びこむ \n水の音]", fmt.printSegmentedContent(trgTc, true, false));

	}

}
