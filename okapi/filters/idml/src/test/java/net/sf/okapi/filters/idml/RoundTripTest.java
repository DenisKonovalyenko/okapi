/*
 * =============================================================================
 * Copyright (C) 2010-2020 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.idml;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import net.sf.okapi.common.EventType;
import net.sf.okapi.common.FileLocation;
import net.sf.okapi.common.FileLocation.In;
import net.sf.okapi.common.FileLocation.Out;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.ZipXMLFileCompare;
import net.sf.okapi.common.filters.InputDocument;
import net.sf.okapi.common.filters.RoundTripComparison;
import net.sf.okapi.common.filterwriter.IFilterWriter;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.common.resource.TextContainer;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

@RunWith(DataProviderRunner.class)
public class RoundTripTest {
    private static final LocaleId DEFAULT_LOCALE = LocaleId.ENGLISH;
    private static final String DEFAULT_CHARSET = StandardCharsets.UTF_8.name();
    private FileLocation root;

    @Before
    public void setUp() {
        root = FileLocation.fromClass(this.getClass());
    }


    @DataProvider
    public static Object[][] testDoubleExtractionProvider() {
        return new Object[][]{
            {"Test00.idml", "okf_idml@ExtractAll.fprm"},
            {"Test01.idml", "okf_idml@ExtractAll.fprm"},
            {"Test02.idml", "okf_idml@ExtractAll.fprm"},
            {"Test03.idml", "okf_idml@ExtractAll.fprm"},

            {"helloworld-1.idml", "okf_idml@ExtractAll.fprm"},
            {"ConditionalText.idml", "okf_idml@ExtractAll.fprm"},

            {"testWithSpecialChars.idml", "okf_idml@ExtractAll.fprm"},

            {"TextPathTest01.idml", "okf_idml@ExtractAll.fprm"},
            {"TextPathTest02.idml", "okf_idml@ExtractAll.fprm"},
            {"TextPathTest03.idml", "okf_idml@ExtractAll.fprm"},
            {"TextPathTest04.idml", "okf_idml@ExtractAll.fprm"},

            {"idmltest.idml", "okf_idml@ExtractAll.fprm"},
            {"idmltest.idml", null},

            {"01-pages-with-text-frames.idml", null},
            {"01-pages-with-text-frames-2.idml", null},
            {"01-pages-with-text-frames-3.idml", null},
            {"01-pages-with-text-frames-4.idml", null},
            {"01-pages-with-text-frames-5.idml", null},
            {"01-pages-with-text-frames-6.idml", null},

            {"02-island-spread-and-threaded-text-frames.idml", null},
            {"03-hyperlink-and-table-content.idml", null},
            {"04-complex-formatting.idml", null},
            {"05-complex-ordering.idml", null},

            {"06-hello-world-12.idml", null},
            {"06-hello-world-13.idml", null},
            {"06-hello-world-14.idml", null},

            {"07-paragraph-breaks.idml", null},

            {"08-conditional-text-and-tracked-changes.idml", null},
            {"change-tracking-3.idml"},
            {"08-direct-story-content.idml", null},

            {"09-footnotes.idml", null},
            {"10-tables.idml", null},

            {"11-xml-structures.idml", "okf_idml@ExtractAll.fprm"},
            {"11-xml-structures.idml", null},

            {"618-objects-without-path-points-and-text.idml", null},
            {"618-anchored-frame-without-path-points.idml", null},
            {"618-MBE3.idml", null},
            {"Bindestrich.idml", null},
            {"756-character-kerning.idml", "okf_idml@IgnoreAll.fprm"},
            {"756-character-tracking.idml", "okf_idml@IgnoreAll.fprm"},
            {"756-character-leading.idml", "okf_idml@IgnoreAll.fprm"},
            {"756-character-baseline-shift.idml", "okf_idml@IgnoreAll.fprm"},
            {"777-character-kerning-method.idml", "okf_idml@IgnoreAll.fprm"},
            {"779-reference-and-tag-styles.idml", "okf_idml@IgnoreAll.fprm"},
            {"923-baselined-formatting.idml", null},
            {"926.idml", "okf_idml@chained-font-mappings.fprm"},
            {"1138.idml", "okf_idml@custom-text-variables-extraction.fprm"},
            {"856-1.idml", null},
            {"856-2.idml", null},
        };
    }

    @Test
    @UseDataProvider("testDoubleExtractionProvider")
    public void testDoubleExtraction(String inputDocumentName, String parametersFileName) {
        final LocaleId locale = LocaleId.fromString("en");
        final List<InputDocument> list = new ArrayList<>();
        list.add(new InputDocument(root.in("/" + inputDocumentName).toString(), parametersFileName));

        final RoundTripComparison rtc = new RoundTripComparison(false); // Do not compare skeleton
        assertTrue(rtc.executeCompare(new IDMLFilter(), list, StandardCharsets.UTF_8.name(), locale, locale, "output"));
    }

    @Test
    public void documentWithChainedFontMappings() {
        final Parameters parameters = new Parameters();
        parameters.fromString(
            "#v1\n" +
            "fontMappings.number.i=3\n" +
            "fontMappings.0.sourceFontPattern=Times.*\n" +
            "fontMappings.0.targetFont=Arial Unicode MS\n" +
            "fontMappings.1.targetLocalePattern=en\n" +
            "fontMappings.1.sourceFontPattern=The Sims Sans\n" +
            "fontMappings.1.targetFont=Arial Unicode MS\n" +
            "fontMappings.2.targetLocalePattern=en\n" +
            "fontMappings.2.sourceFontPattern=Arial Unicode MS\n" +
            "fontMappings.2.targetFont=Meiryo\n"
        );
        roundTripAndCheck(parameters, "926.idml", "926-chained.idml");
    }

    @Test
    public void documentsWithDefaultParameters() {
        roundTripAndCheck(new Parameters(), "926.idml", "926.idml");
    }

    @Test
    public void emptyTargetsMerged() {
        final FileLocation.In input = root.in("/629.idml");
        final FileLocation.Out actualOutput = root.out("/actual/629.idml");
        final FileLocation.In expectedOutput = root.in("/expected/629.idml");
        try (final RawDocument rawDocument = new RawDocument(input.asUri(), DEFAULT_CHARSET, DEFAULT_LOCALE);
             final IDMLFilter filter = new IDMLFilter()) {
            filter.setParameters(new Parameters());
            filter.open(rawDocument, true);
            try (final IFilterWriter filterWriter = filter.createFilterWriter()) {
                filterWriter.setOutput(actualOutput.toString());
                filterWriter.setOptions(LocaleId.FRENCH, StandardCharsets.UTF_8.name());
                filter.forEachRemaining(event -> {
                    if (EventType.TEXT_UNIT.equals(event.getEventType())) {
                        final ITextUnit tu = event.getTextUnit();
                        if ("Second paragraph.".equals(tu.getSource().getCodedText())
                            || "Last paragraph.".equals(tu.getSource().getCodedText())) {
                            event.getTextUnit().setTarget(LocaleId.FRENCH, new TextContainer());
                        }
                    }
                    filterWriter.handleEvent(event);
                });
            }
        }
        final ZipXMLFileCompare zfc = new ZipXMLFileCompare();
        assertTrue(zfc.compareFiles(actualOutput.toString(), expectedOutput.toString()));
    }

    @Test
    public void specialCharactersExtractedAndMerged() {
        roundTripAndCheck(new Parameters(), "175-special-characters.idml", "175-special-characters.idml");
    }

    @Test
    public void customTextVariablesExtractedAndMerged() {
        final Parameters parameters = new Parameters();
        parameters.setExtractCustomTextVariables(true);
        roundTripAndCheck(parameters, "1138.idml", "1138.idml");
    }

    @Test
    public void indexTopicsExtractedAndMerged() {
        final Parameters parameters = new Parameters();
        parameters.setExtractIndexTopics(true);
        roundTripAndCheck(parameters, "1139.idml", "1139.idml");
    }

    @Test
    public void endNotesExtractedAndMerged() {
        roundTripAndCheck(new Parameters(), "856-1.idml", "856-1.idml");
        roundTripAndCheck(new Parameters(), "856-2.idml", "856-2.idml");
    }

    @Test
    public void hyperlinkTextSourcesExtractedAndMerged() {
        final Parameters parameters = new Parameters();
        parameters.setExtractHyperlinkTextSourcesInline(false);
        roundTripAndCheck(parameters, "1179-0.idml", "1179-0.idml");
        roundTripAndCheck(parameters, "1179-1.idml", "1179-1.idml");
        roundTripAndCheck(parameters, "1179-2.idml", "1179-2.idml");
        roundTripAndCheck(parameters, "1179-3.idml", "1179-3.idml");
        roundTripAndCheck(parameters, "1179-4.idml", "1179-4.idml");
        roundTripAndCheck(parameters, "03-hyperlink-and-table-content.idml", "03-hyperlink-and-table-content.idml");
        parameters.setExtractHyperlinkTextSourcesInline(true);
        roundTripAndCheck(parameters, "1179-0.idml", "1179-0.idml");
        roundTripAndCheck(parameters, "1179-1.idml", "1179-1.idml");
        roundTripAndCheck(parameters, "1179-2.idml", "1179-2.idml");
        roundTripAndCheck(parameters, "1179-3.idml", "1179-3.idml");
        roundTripAndCheck(parameters, "1179-4.idml", "1179-4.idml");
        roundTripAndCheck(parameters, "03-hyperlink-and-table-content.idml", "03-hyperlink-and-table-content.idml");
    }

    @Test
    public void externalHyperlinksExtractedAndMerged() {
        final Parameters parameters = new Parameters();
        parameters.setExtractExternalHyperlinks(true);
        roundTripAndCheck(parameters, "03-hyperlink-and-table-content.idml", "03-hyperlink-and-table-content.idml");
    }

    @Test
    public void emptyContentStylesPreserved() {
        final Parameters parameters = new Parameters();
        roundTripAndCheck(parameters, "1369-empty-paragraph-styles.idml", "1369-empty-paragraph-styles.idml");
        roundTripAndCheck(parameters, "1369-empty-paragraph-in-table-cell-styles.idml", "1369-empty-paragraph-in-table-cell-styles.idml");
    }

    private void roundTripAndCheck(final Parameters parameters, final String documentName, final String goldDocumentName) {
        final In input = root.in("/" + documentName);
        final Out actualOutput = root.out("/actual/" + goldDocumentName);
        final In expectedOutput = root.in("/expected/" + goldDocumentName);
		try (final RawDocument rawDocument = new RawDocument(input.asUri(), DEFAULT_CHARSET, DEFAULT_LOCALE);
				final IDMLFilter filter = new IDMLFilter()) {
			filter.setParameters(parameters);
			filter.open(rawDocument, true);
			try (final IFilterWriter filterWriter = filter.createFilterWriter()) {
				filterWriter.setOutput(actualOutput.toString());
				filterWriter.setOptions(DEFAULT_LOCALE, DEFAULT_CHARSET);
				filter.forEachRemaining(filterWriter::handleEvent);
			}
		}
		final ZipXMLFileCompare zfc = new ZipXMLFileCompare();
		assertTrue(zfc.compareFiles(actualOutput.toString(), expectedOutput.toString()));
	}
}
