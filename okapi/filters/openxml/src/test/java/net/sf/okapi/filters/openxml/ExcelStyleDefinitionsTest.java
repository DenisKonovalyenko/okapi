package net.sf.okapi.filters.openxml;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Collections;

import org.assertj.core.api.Assertions;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import net.sf.okapi.common.FileLocation;

import javax.xml.stream.XMLStreamException;

@RunWith(JUnit4.class)
public class ExcelStyleDefinitionsTest {
	private final XMLFactories xmlfactories;
	private final FileLocation root;
	private final PresetColorValues presetColorValues;
	private final PresetColorValues highlightColorValues;
	private Theme theme;

	public ExcelStyleDefinitionsTest() {
		this.xmlfactories = new XMLFactoriesForTest();
		this.root = FileLocation.fromClass(getClass());
		this.presetColorValues = new PresetColorValues.Default();
		this.highlightColorValues = new PresetColorValues.Default(Collections.emptyList());
	}

	@Before
	public void setUp() throws XMLStreamException, IOException {
		this.theme = new Theme.Default(new PresetColorValues.Default());
		try (final Reader reader = new InputStreamReader(this.root.in("/xlsx_parts/theme1.xml").asInputStream(), OpenXMLFilter.ENCODING)) {
			this.theme.readWith(this.xmlfactories.getInputFactory().createXMLEventReader(reader));
		}
	}

	@Test
	public void argbForegroundColorsDetermined() throws Exception {
		final StyleDefinitions styleDefinitions = new ExcelStyleDefinitions();
		try (final Reader reader = new InputStreamReader(this.root.in("/xlsx_parts/rgb_styles.xml").asInputStream(), OpenXMLFilter.ENCODING)) {
			final SystemColorValues systemColorValues = new SystemColorValues.Default();
			styleDefinitions.readWith(
				new ExcelStyleDefinitionsReader(
					new ConditionalParameters(),
					this.xmlfactories.getEventFactory(),
					this.presetColorValues,
                    this.highlightColorValues,
					systemColorValues,
					new IndexedColors.Default(systemColorValues),
					this.theme,
					this.xmlfactories.getInputFactory().createXMLEventReader(reader)
				)
			);
		}
		Assertions.assertThat(
			styleDefinitions.combinedDifferentialFormatFor(0)
				.fill().pattern().foregroundColor().value().asRgb()
		).isEqualTo("");
		Assertions.assertThat(
			styleDefinitions.combinedDifferentialFormatFor(1)
				.fill().pattern().foregroundColor().value().asRgb()
		).isEqualTo("800000");
		Assertions.assertThat(
			styleDefinitions.combinedDifferentialFormatFor(2)
				.fill().pattern().foregroundColor().value().asRgb()
		).isEqualTo("FF0000");
		Assertions.assertThat(
			styleDefinitions.combinedDifferentialFormatFor(3)
				.fill().pattern().foregroundColor().value().asRgb()
		).isEqualTo("FF6600");
		Assertions.assertThat(
			styleDefinitions.combinedDifferentialFormatFor(4)
				.fill().pattern().foregroundColor().value().asRgb()
		).isEqualTo("FFFF00");
		Assertions.assertThat(
			styleDefinitions.combinedDifferentialFormatFor(5)
				.fill().pattern().foregroundColor().value().asRgb()
		).isEqualTo("CCFFCC");
		Assertions.assertThat(
			styleDefinitions.combinedDifferentialFormatFor(6)
				.fill().pattern().foregroundColor().value().asRgb()
		).isEqualTo("008000");
		Assertions.assertThat(
			styleDefinitions.combinedDifferentialFormatFor(7)
				.fill().pattern().foregroundColor().value().asRgb()
		).isEqualTo("3366FF");
		Assertions.assertThat(
			styleDefinitions.combinedDifferentialFormatFor(8)
				.fill().pattern().foregroundColor().value().asRgb()
		).isEqualTo("0000FF");
		Assertions.assertThat(
			styleDefinitions.combinedDifferentialFormatFor(9)
				.fill().pattern().foregroundColor().value().asRgb()
		).isEqualTo("000090");
		Assertions.assertThat(
			styleDefinitions.combinedDifferentialFormatFor(10)
				.fill().pattern().foregroundColor().value().asRgb()
		).isEqualTo("660066");
	}

	@Test
	public void optionalFillIdOfCellFormatHandled() throws Exception {
		final StyleDefinitions styleDefinitions = new ExcelStyleDefinitions();
		try (final Reader reader = new InputStreamReader(this.root.in("/xlsx_parts/missing_fillId.xml").asInputStream(), OpenXMLFilter.ENCODING)) {
			final SystemColorValues systemColorValues = new SystemColorValues.Default();
			styleDefinitions.readWith(
				new ExcelStyleDefinitionsReader(
					new ConditionalParameters(),
					this.xmlfactories.getEventFactory(),
					this.presetColorValues,
					this.highlightColorValues,
					systemColorValues,
					new IndexedColors.Default(systemColorValues),
					this.theme,
					this.xmlfactories.getInputFactory().createXMLEventReader(reader)
				)
			);
		}
		Assertions.assertThat(
			styleDefinitions.combinedDifferentialFormatFor(2).fill().pattern().foregroundColor().value().asRgb()
		).isEqualTo("");
	}
}
