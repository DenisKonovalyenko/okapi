/*
 * =============================================================================
 * Copyright (C) 2010-2021 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

interface Table {
    String COLUMN = "tableColumn";
    // @todo: consider table names translation
    String name();
    CellReferencesRange cellReferencesRange();
    Set<String> columnNames();
    void readWith(final XMLEventReader reader) throws XMLStreamException;
    Markup asMarkup();

    final class Default implements Table {
        private static final String TABLE = "table";
        private static final QName NAME = new QName("name");
        private static final QName REF = new QName("ref");
        private final XMLEventFactory eventFactory;
        private final String worksheetName;
        private final Set<String> columnNames;
        private MarkupBuilder markupBuilder;
        private String name;
        private CellReferencesRange cellReferencesRange;

        Default(final XMLEventFactory eventFactory, final String worksheetName) {
            this(eventFactory, worksheetName, new HashSet<>());
        }

        Default(
            final XMLEventFactory eventFactory,
            final String worksheetName,
            final Set<String> columnNames
        ) {
            this.eventFactory = eventFactory;
            this.worksheetName = worksheetName;
            this.columnNames = columnNames;
        }

        @Override
        public String name() {
            return this.name;
        }

        @Override
        public CellReferencesRange cellReferencesRange() {
            return this.cellReferencesRange;
        }

        @Override
        public Set<String> columnNames() {
            return this.columnNames;
        }

        @Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
            this.markupBuilder = new MarkupBuilder(new Markup.General(new ArrayList<>()));
            while (reader.hasNext()) {
                final XMLEvent e = reader.nextEvent();
                if (e.isStartElement()) {
                    final StartElement se = e.asStartElement();
                    if (Default.TABLE.equals(se.getName().getLocalPart())
                        && Namespace.PREFIX_EMPTY.equals(se.getName().getPrefix())) {
                        this.cellReferencesRange = new CellReferencesRange(
                            XMLEventHelpers.getAttributeValue(se, Default.REF)
                        );
                        this.name = XMLEventHelpers.getAttributeValue(se, Default.NAME);
                    } else if (Table.COLUMN.equals(se.getName().getLocalPart())) {
                        this.columnNames.add(XMLEventHelpers.getAttributeValue(se, Default.NAME));
                        this.markupBuilder.add(
                            new MarkupComponent.Start(
                                this.eventFactory,
                                se,
                                new MarkupComponent.Context.Default(this.name)
                            )
                        );
                        continue;
                    } else if (Formula.CALCULATED_COLUMN.equals(se.getName().getLocalPart())
                        || Formula.TOTALS_ROW.equals(se.getName().getLocalPart())) {
                        final Formula formula = new Formula.Default(
                            se,
                            new MarkupComponent.Context.Default(this.worksheetName)
                        );
                        formula.readWith(reader);
                        this.markupBuilder.add(formula);
                        continue;
                    }
                } else if (e.isEndElement()) {
                    final EndElement ee = e.asEndElement();
                    if (Table.COLUMN.equals(ee.getName().getLocalPart())) {
                        this.markupBuilder.add(new MarkupComponent.End(ee));
                        continue;
                    }
                }
                this.markupBuilder.add(e);
            }
        }

        @Override
        public Markup asMarkup() {
            return this.markupBuilder.build();
        }
    }
}
