/*===========================================================================
  Copyright (C) 2016-2017 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.filters.openxml;

final class RunCode {
    private final int id;
    private final String type;
    private final RunPropertiesPairWithDetectedRunFonts runPropertiesPairWithDetectedRunFonts;

    RunCode(
        final int id,
        final String type,
        final RunPropertiesPairWithDetectedRunFonts runPropertiesPairWithDetectedRunFonts
    ) {
        this.id = id;
        this.type = type;
        this.runPropertiesPairWithDetectedRunFonts = runPropertiesPairWithDetectedRunFonts;
    }

    int id() {
        return id;
    }

    String type() {
        return type;
    }

    RunPropertiesPairWithDetectedRunFonts runPropertiesPair() {
        return this.runPropertiesPairWithDetectedRunFonts;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "(" + this.id + ", " + this.type + ", " + this.runPropertiesPairWithDetectedRunFonts.toString() + ")";
    }
}