/*
 * =============================================================================
 * Copyright (C) 2010-2021 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

interface PivotCacheDefinition {
    String WORKSHEET_SOURCE = "worksheetSource";
    String CACHE_FIELD = "cacheField";
    String sheetNameSource();
    CellReferencesRange cellReferencesRangeSource();
    Set<String> cacheFieldNames();
    void readWith(final XMLEventReader reader) throws XMLStreamException;
    Markup asMarkup();

    final class Default implements PivotCacheDefinition {
        private static final String EMPTY = "";
        private static final QName NAME = new QName("name");
        private static final QName SHEET = new QName("sheet");
        private static final QName REF = new QName("ref");
        private static final String CACHE_SOURCE = "cacheSource";
        private static final QName TYPE = new QName("type");
        private static final String WORKSHEET = "worksheet";
        private final XMLEventFactory eventFactory;
        private final Set<String> cacheFieldNames;
        private MarkupBuilder markupBuilder;
        private String cacheSourceType;
        private String namedSource;
        private String sheetNameSource;
        private CellReferencesRange cellReferencesRangeSource;

        Default(final XMLEventFactory eventFactory) {
            this(eventFactory, new HashSet<>());
        }

        Default(final XMLEventFactory eventFactory, final Set<String> cacheFieldNames) {
            this.eventFactory = eventFactory;
            this.cacheFieldNames = cacheFieldNames;
        }

        @Override
        public String sheetNameSource() {
            return this.sheetNameSource;
        }

        @Override
        public CellReferencesRange cellReferencesRangeSource() {
            return this.cellReferencesRangeSource;
        }

        @Override
        public Set<String> cacheFieldNames() {
            return this.cacheFieldNames;
        }

        @Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
            this.namedSource = Default.EMPTY;
            this.sheetNameSource = EMPTY;
            this.cellReferencesRangeSource = new CellReferencesRange(EMPTY);
            boolean cacheSourceDefined = false;
            this.markupBuilder = new MarkupBuilder(new Markup.General(new ArrayList<>()));
            while (reader.hasNext()) {
                final XMLEvent e = reader.nextEvent();
                if (e.isStartElement()) {
                    final StartElement se = e.asStartElement();
                    if (Default.CACHE_SOURCE.equals(se.getName().getLocalPart())) {
                        this.cacheSourceType = XMLEventHelpers.getAttributeValue(se, Default.TYPE);
                    } else if (Default.WORKSHEET.equals(this.cacheSourceType) && Default.WORKSHEET_SOURCE.equals(se.getName().getLocalPart())) {
                        defineCacheSource(se);
                        cacheSourceDefined = true;
                        if (!this.sheetNameSource.isEmpty()) {
                            this.markupBuilder.add(
                                new MarkupComponent.Start(
                                    this.eventFactory,
                                    se,
                                    new MarkupComponent.Context.Default(Default.EMPTY)
                                )
                            );
                            continue;
                        }
                    } else if (cacheSourceDefined && Default.CACHE_FIELD.equals(se.getName().getLocalPart())) {
                        this.cacheFieldNames.add(XMLEventHelpers.getAttributeValue(se, Default.NAME));
                        this.markupBuilder.add(
                            new MarkupComponent.Start(
                                this.eventFactory,
                                se,
                                new MarkupComponent.Context.Default(cacheSource())
                            )
                        );
                        continue;
                    }
                } else if (e.isEndElement()) {
                    final EndElement ee = e.asEndElement();
                    if (!this.sheetNameSource.isEmpty() && Default.WORKSHEET_SOURCE.equals(ee.getName().getLocalPart())) {
                        this.markupBuilder.add(new MarkupComponent.End(ee));
                        continue;
                    } else if (cacheSourceDefined && Default.CACHE_FIELD.equals(ee.getName().getLocalPart())) {
                        this.markupBuilder.add(new MarkupComponent.End(ee));
                        continue;
                    }
                }
                this.markupBuilder.add(e);
            }
        }

        private void defineCacheSource(final StartElement se) {
            this.namedSource = XMLEventHelpers.getAttributeValue(se, Default.NAME, Default.EMPTY);
            this.sheetNameSource = XMLEventHelpers.getAttributeValue(se, Default.SHEET, Default.EMPTY);
            this.cellReferencesRangeSource = new CellReferencesRange(
                XMLEventHelpers.getAttributeValue(se, Default.REF, Default.EMPTY)
            );
        }

        private String cacheSource() {
            final String name;
            if (!this.namedSource.isEmpty()) {
                name = this.namedSource;
            } else if (!this.sheetNameSource.isEmpty()) {
                name = this.sheetNameSource;
            } else {
                name = EMPTY;
            }
            return name;
        }

        @Override
        public Markup asMarkup() {
            return this.markupBuilder.build();
        }
    }
}
