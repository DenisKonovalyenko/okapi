/*===========================================================================
  Copyright (C) 2023 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/
package net.sf.okapi.filters.cascadingfilter;

import net.sf.okapi.filters.cascadingfilter.CascadingFilter;

import java.util.List;

/**
 * Warning: Does not implement {@link net.sf.okapi.common.IParameters}
 *
 * Primary use case is to pass parameters to the filter directly using the factory method.
 * It's possible this could be converted to true a {@link net.sf.okapi.common.IParameters} implementation
 * in the future but would offer little benefit as {@link CascadingFilter} isn't implemented as a true standalone
 * filter, more of a generator for a sequence of filtering {@link net.sf.okapi.common.pipeline.IPipelineStep}.
 */
public class FilterParameters {
    private final String primaryConfigId;
    private final List<String> subFilterConfigIds;
    private final String primaryConfig;
    private final List<String> subFilterConfigs;

    public FilterParameters(String primaryConfigId, List<String> subFilterConfigIds, String primaryConfig, List<String> subFilterConfigs) {
        this.primaryConfigId = primaryConfigId;
        this.subFilterConfigIds = subFilterConfigIds;
        this.primaryConfig = primaryConfig;
        this.subFilterConfigs = subFilterConfigs;
    }

    public String getPrimaryConfigId() {
        return primaryConfigId;
    }

    public List<String> getSubFilterConfigIds() {
        return subFilterConfigIds;
    }

    public String getPrimaryConfig() {
        return primaryConfig;
    }

    public List<String> getSubFilterConfigs() {
        return subFilterConfigs;
    }
}
