/*
 * ====================================================================
 *   Copyright (C) 2023 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 * =====================================================================
 */

package net.sf.okapi.filters.messageformat;

import com.ibm.icu.text.MessagePattern;
import com.ibm.icu.text.MessagePatternUtil;
import net.sf.okapi.common.BOMNewlineEncodingDetector;
import net.sf.okapi.common.Event;
import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.exceptions.OkapiBadFilterInputException;
import net.sf.okapi.common.exceptions.OkapiIOException;
import net.sf.okapi.common.exceptions.OkapiUnsupportedEncodingException;
import net.sf.okapi.common.filters.AbstractSubFilter;
import net.sf.okapi.common.filters.FilterConfiguration;
import net.sf.okapi.common.resource.Code;
import net.sf.okapi.common.resource.Property;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.common.resource.TextFragment;
import net.sf.okapi.common.skeleton.GenericSkeleton;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * Implements the IFilter interface for ICU MessageFormat strings. This is meant to be
 * used as a subfilter.
 */
public class MessageFormatFilter extends AbstractSubFilter implements INodeVisitor {
    public static final String MIME_TYPE = "text/x-messageformat";
    public static final String SELECT_ARG_NAME = "select-arg-name";
    public static final String SELECT_TYPE = "select-type";
    public static final String ORIGINAL_STRING = "original-string";
    public static final String FORMATTED = "formatted";

    private MessageFormatEventBuilder eventBuilder;
    private RawDocument input;

    private MessageFormatParser parser;
    private Parameters params;

    public MessageFormatFilter() {
        super();
        // default converter for standalone use
        setMimeType(MIME_TYPE);
        setMultilingual(false);
        setName("okf_messageformat");
        setDisplayName("Message Format Filter");
        // must be called *after* parameters is initialized
        addConfiguration(new FilterConfiguration(getName(), MIME_TYPE, getClass().getName(), "ICU Message Format",
                "ICU Message Format Text", null, null));
        addConfiguration(new FilterConfiguration(getName() + "-json", MIME_TYPE, getClass().getName(), "ICU Message Format",
                "ICU Message Format From JSON", "okf_messageformat@json.fprm", null));
        addConfiguration(new FilterConfiguration(getName() + "-yaml", MIME_TYPE, getClass().getName(), "ICU Message Format",
                "ICU Message Format From YAML", "okf_messageformat@yaml.fprm", null));
        params = new Parameters();
        setParameters(params);
    }

    /**
     * Reads input from the {@link BOMNewlineEncodingDetector} and returns it as a StringBuilder.
     *
     * @param detector The {@link BOMNewlineEncodingDetector} to read input from.
     * @param encoding The encoding to use for reading the input.
     * @return A StringBuilder containing the input.
     * @throws OkapiUnsupportedEncodingException If the specified encoding is not supported.
     * @throws OkapiIOException                  If there is an error reading the input.
     */
    private static StringBuilder getInputAsString(BOMNewlineEncodingDetector detector, String encoding) {
        StringBuilder tmp = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(detector.getInputStream(), encoding))) {
            char[] buf = new char[4096];
            int count;
            while ((count = reader.read(buf)) != -1) {
                tmp.append(buf, 0, count);
            }
        } catch (UnsupportedEncodingException e) {
            throw new OkapiUnsupportedEncodingException(String.format("The encoding '%s' is not supported.", encoding),
                    e);
        } catch (IOException e) {
            throw new OkapiIOException("Error reading the input.", e);
        }
        return tmp;
    }

    @Override
    public void close() {
        super.close();
        if (input != null) {
            input.close();
        }
        try {
            if (parser != null) {
                parser.close();
            }
        } catch (Exception e) {
            throw new OkapiIOException(e);
        }
    }

    @Override
    public boolean hasNext() {
        return eventBuilder.hasNext();
    }

    /**
     * Retrieves the next event from the event builder.
     * If there are queued events, it returns the next event from the queue.
     * Otherwise, it iterates through the token iterator and processes each token accordingly.
     * If there are no more tokens, it flushes any remaining temporary events and adds an end filter event.
     * Finally, it converts the next event from the event builder and returns it.
     *
     * @return The next event.
     */
    @Override
    public Event next() {
        Event e = convertEvent(eventBuilder.next());
        if (e.isStartSubfilter() && getParameters().isPrettyPrint()) {
            e.getStartSubfilter().setSkelWriter(new MessageFormatSkeletonWriter(e.getStartSubfilter(), getTrgLoc()));
        }
        return e;
    }

    @Override
    public void open(RawDocument input) {
        open(input, true);
    }

    /**
     * Opens a raw document and initializes the necessary properties.
     *
     * @param input            The raw document to be opened.
     * @param generateSkeleton Flag indicating whether to generate a skeleton.
     */
    @Override
    public void open(RawDocument input, boolean generateSkeleton) {
        super.open(input, generateSkeleton);
        this.input = input;

        BOMNewlineEncodingDetector detector = new BOMNewlineEncodingDetector(input.getStream(), "UTF-8");
        detector.detectAndRemoveBom();
        input.setEncoding(detector.getEncoding());
        String encoding = input.getEncoding();
        setOptions(input.getSourceLocale(), input.getTargetLocale() == null ? input.getSourceLocale() : input.getTargetLocale(), encoding, generateSkeleton);

        // Create EventBuilder with document name as rootId
        if (eventBuilder == null) {
            eventBuilder = new MessageFormatEventBuilder(getParentId(), this);
        } else {
            eventBuilder.reset(getParentId(), this);
        }
        eventBuilder.setMimeType(getMimeType());
        eventBuilder.setPreserveWhitespace(true);

        // Compile code finder rules
        if (params.getUseCodeFinder()) {
            params.getCodeFinder().compile();
            eventBuilder.setCodeFinder(params.getCodeFinder());
        }

        try {
            String inputAsString = getInputAsString(detector, encoding).toString();
            parse(inputAsString);
            Event startDocument = createStartFilterEvent();
            startDocument.getStartDocument().setProperty(new Property(ORIGINAL_STRING, inputAsString, Property.DISPLAY_ONLY));
            String formatted = parser.toFormatted(getSrcLoc());
            startDocument.getStartDocument().setProperty(new Property(FORMATTED, formatted, Property.DISPLAY_ONLY));
            eventBuilder.addFilterEvent(startDocument);
            visit(parser.getRootNode());
            eventBuilder.flushRemainingTempEvents();
            eventBuilder.addFilterEvent(createEndFilterEvent());
        } catch (Exception e) {
            throw new OkapiBadFilterInputException("Error reading Message Format String", e);
        }
    }

    @Override
    public Parameters getParameters() {
        return params;
    }

    @Override
    public void setParameters(IParameters params) {
        this.params = (Parameters) params;
    }

    /**
     * Parses the input string and sets up the event builder and parser.
     *
     * @param inputString The input string to parse.
     * @throws IOException               If an I/O error occurs.
     * @throws IllegalArgumentException  if the message string is invalid
     * @throws IndexOutOfBoundsException if the message string is invalid
     * @throws NumberFormatException     if the message string is invalid
     */
    private void parse(String inputString) throws Exception {
        parser = new MessageFormatParser(inputString);
        if (getParameters().isNormalize()) {
            try {
                parser.normalize();
            } catch (Exception e) {
                throw new OkapiBadFilterInputException("Error validating message string with new plural forms", e);
            }
        }

        if (getParameters().isAddPluralForms()) {
            try {
                parser.addPluralForms(getSrcLoc(), getTrgLoc());
            } catch (Exception e) {
                throw new OkapiBadFilterInputException("Error validating message string with new plural forms", e);
            }
        }
    }

    @Override
    public void visit(MessagePatternUtil.MessageNode node) {
        for (MessagePatternUtil.MessageContentsNode contentsNode : node.getContents()) {
            visit(contentsNode);
        }
    }

    @Override
    public void visit(MessagePatternUtil.MessageContentsNode node) {
        switch (node.getType()) {
            case TEXT:
                visit((MessagePatternUtil.TextNode) node);
                break;
            case ARG:
                visit((MessagePatternUtil.ArgNode) node);
                break;
            case REPLACE_NUMBER:
                if (!eventBuilder.isCurrentTextUnit()) {
                    eventBuilder.startTextUnit();
                }
                Code c = new Code(TextFragment.TagType.PLACEHOLDER,
                        node.getType().name(),
                        "#");
                c.setType(node.getType().name());
                c.setDisplayText(node.toString());
                eventBuilder.addToTextUnit(c);
                break;
        }
    }

    @Override
    public void visit(MessagePatternUtil.TextNode node) {
        String literalText = node.getText();
        if (!eventBuilder.isCurrentTextUnit()) {
            eventBuilder.startTextUnit();
        }
        eventBuilder.addToTextUnit(literalText);
    }

    @Override
    public void visit(MessagePatternUtil.ArgNode node) {
        StringBuilder msg = new StringBuilder();
        msg.append("{");
        if (node.getArgType() == MessagePattern.ArgType.NONE) {
            msg.append(String.format("%s", node.getName()));
        } else {
            msg.append(String.format("%s, %s", node.getName(), node.getTypeName()));
            if (node.getSimpleStyle() != null) {
                msg.append(", ");
                msg.append(node.getSimpleStyle().trim());
            }
        }
        MessagePatternUtil.ComplexArgStyleNode complexNode = node.getComplexStyle();
        if (complexNode == null) {
            // simple node
            if (!eventBuilder.isCurrentTextUnit()) {
                eventBuilder.startTextUnit();
            }
            Code c = new Code(TextFragment.TagType.PLACEHOLDER, node.getTypeName(), msg.append("}").toString());
            c.setType(node.getType().name());
            c.setDisplayText(node.getName());
            eventBuilder.addToTextUnit(c);
        } else {
            // complex node
            if (complexNode.hasExplicitOffset()) {
                msg.append(String.format(", offset:%d", (int) complexNode.getOffset()));
            } else {
                msg.append(",");
            }
            eventBuilder.startGroup(new GenericSkeleton(msg.toString()), node.getName());
            eventBuilder.addToGroup(new Property(MessageFormatFilter.SELECT_TYPE, node.getArgType().name(), Property.DISPLAY_ONLY));
            eventBuilder.addToGroup(new Property(MessageFormatFilter.SELECT_ARG_NAME, node.getName(), Property.DISPLAY_ONLY));

            visit(complexNode, node.getName().trim());
            eventBuilder.endGroup(new GenericSkeleton("}"));
        }
    }

    private void visit(MessagePatternUtil.VariantNode node, String context) {
        eventBuilder.startTextUnit(new GenericSkeleton(String.format(" %s {", node.getSelector().trim())));
        String name = String.format("%s_%s", context, node.getSelector().trim());
        eventBuilder.setTextUnitType(name);
        eventBuilder.setTextUnitName(name);
        if (node.getSelector() != null) {
            eventBuilder.addToTextUnit(new Property(MessageFormatFilter.SELECT_ARG_NAME, node.getSelector().trim(), Property.DISPLAY_ONLY));
        }
        visit(node.getMessage());
        eventBuilder.endTextUnit(new GenericSkeleton("}"));
    }

    private void visit(MessagePatternUtil.ComplexArgStyleNode node, String context) {
        if (node.getArgType() == MessagePattern.ArgType.CHOICE) {
            // FIXME: deprecated message format should we repair with select or plural?
            throw new OkapiBadFilterInputException("CHOICE format is deprecated and is not supported");
        }

        List<MessagePatternUtil.VariantNode> variants = node.getVariants();
        for (MessagePatternUtil.VariantNode variant : variants) {
            visit(variant, context);
        }
    }
}
