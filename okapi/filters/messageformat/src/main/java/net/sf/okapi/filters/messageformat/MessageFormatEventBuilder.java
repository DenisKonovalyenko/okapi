package net.sf.okapi.filters.messageformat;

import net.sf.okapi.common.encoder.EncoderManager;
import net.sf.okapi.common.filters.EventBuilder;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.common.filters.InlineCodeFinder;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.TextFragment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MessageFormatEventBuilder extends EventBuilder {
    private InlineCodeFinder codeFinder;
    private final EncoderManager encoderManager;

    public MessageFormatEventBuilder(String rootId, IFilter subFilter) {
        super(rootId, subFilter);
        codeFinder = null;
        this.encoderManager = subFilter.getEncoderManager();
    }

    @Override
    protected ITextUnit postProcessTextUnit(ITextUnit textUnit) {
        TextFragment text = textUnit.getSource().getFirstContent();
        text.setCodedText(text.getCodedText());

        if (codeFinder != null) {
            encoderManager.updateEncoder(textUnit.getMimeType());
            codeFinder.process(text);
        }
        text.setCodedText(text.getCodedText());
        return textUnit;
    }

    public void setCodeFinder(InlineCodeFinder codeFinder) {
        this.codeFinder = codeFinder;
    }
}
