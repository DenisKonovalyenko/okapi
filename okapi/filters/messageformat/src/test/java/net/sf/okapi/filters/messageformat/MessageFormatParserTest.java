/*
 * ====================================================================
 *   Copyright (C) $time.year by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 * =====================================================================
 */

package net.sf.okapi.filters.messageformat;

import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.StringUtil;
import net.sf.okapi.common.exceptions.OkapiBadFilterInputException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class MessageFormatParserTest {

    @Test
    public void testInvalidMessageFormat() {
        try (MessageFormatParser p = new MessageFormatParser("Invalid message {missing_closing_brace")) {
            fail("Expected an IllegalArgumentException");
        } catch (Exception e) {
            // Expected exception
        }
    }

    @Test
    public void testSkipSyntaxApostrophe() throws Exception {
        String message = "I have '{count, plural, one {# apple} other {# apples}}'";
        try (MessageFormatParser p = new MessageFormatParser(message)) {
            assertEquals(StringUtil.collapseWhitespace(message), StringUtil.collapseWhitespace(p.toString()));
        }
    }

    @Test
    public void testSkipSyntaxEmbedded() throws Exception {
        String message = "'{0, plural, one {You have {1, plural, one {# apple} other {# apples}}} other {You and # others have {1, plural, one {# apple} other {# apples}}}}'";
        try (MessageFormatParser p = new MessageFormatParser(message)) {
            assertEquals(StringUtil.collapseWhitespace(message), StringUtil.collapseWhitespace(p.toString()));
        }
    }

    @Test
    public void testSkipSyntaxEscapedBraces() throws Exception {
        String message = "This is a '{{literal text}}' in the message.";
        try (MessageFormatParser p = new MessageFormatParser(message)) {
            assertEquals(StringUtil.collapseWhitespace(message), StringUtil.collapseWhitespace(p.toString()));
        }
    }

    @Test
    public void testSkipSyntaxQuotedText() throws Exception {
        String message = "He said, '{quote, select, yes {Yes} no {No}}'";
        try (MessageFormatParser p = new MessageFormatParser(message)) {
            assertEquals(StringUtil.collapseWhitespace(message), StringUtil.collapseWhitespace(p.toString()));
        }
    }

    @Test
    public void testSkipSyntaxComplexPattern() throws Exception {
        String message = "This is a '{complex, choice, simple {simple pattern} complex {complex pattern}}' message.";
        try (MessageFormatParser p = new MessageFormatParser(message)) {
            assertEquals(StringUtil.collapseWhitespace(message), StringUtil.collapseWhitespace(p.toString()));
        }
    }

    @Test
    public void testOffset() throws Exception {
        String message = "{n, plural, offset:2 =0 {No (#) trains are available} one {# train is available} other {# trains are available}}";
        try (MessageFormatParser p = new MessageFormatParser(message)) {
            assertEquals(StringUtil.collapseWhitespace(message), StringUtil.collapseWhitespace(p.toString()));
        }
    }

    @Test
    public void testRussian() throws Exception {
        String message = "{members, plural, \n" +
                "=0 {Нет доступных членов} \n" +
                "one {Есть один член.} \n" +
                "few {Имеется # члена.} \n" +
                "many {Есть # членов.} \n" +
                "other {# члена.}}";
        try (MessageFormatParser p = new MessageFormatParser()) {
            p.parse(message);
            assertEquals(StringUtil.collapseWhitespace(message), StringUtil.collapseWhitespace(p.toString()));
        }
    }

    @Test
    public void testWithPlural() throws Exception {
        String message = "Text {count, plural, zero {foo} =1 {bar} =2 {baz} other {bazinga}} {foo} {0} End";
        try (MessageFormatParser p = new MessageFormatParser()) {
            p.parse(message);
            assertEquals(StringUtil.collapseWhitespace(message), StringUtil.collapseWhitespace(p.toString()));
        }
    }

    @Test
    public void testWithSimpleMessage() throws Exception {
        String message = "Hello, {name}!";
        try (MessageFormatParser p = new MessageFormatParser()) {
            p.parse(message);
            assertEquals(StringUtil.collapseWhitespace(message), StringUtil.collapseWhitespace(p.toString()));
        }
    }

    @Test
    public void testWithPluralMessage() throws Exception {
        String message = "{count, plural, one {There is one item.} other {There are # items.}}";
        try (MessageFormatParser p = new MessageFormatParser()) {
            p.parse(message);
            assertEquals(StringUtil.collapseWhitespace(message), StringUtil.collapseWhitespace(p.toString()));
        }
    }

    @Test
    public void testWithNestedMessage() throws Exception {
        String message = "{level, select, info {Information: {message}} error {Error: {message}} other {Status: {message}}}";
        try (MessageFormatParser p = new MessageFormatParser()) {
            p.parse(message);
            assertEquals(StringUtil.collapseWhitespace(message), StringUtil.collapseWhitespace(p.toString()));
        }
    }

    @Test
    public void testWithComplexGenderMessage() throws Exception {
        String message = "{userGender, select, male {{user} has} female {{user} has} other {{user} has}} invited {guestCount, plural, one {one guest} other {# guests}} to the event.";
        try (MessageFormatParser p = new MessageFormatParser()) {
            p.parse(message);
            assertEquals(StringUtil.collapseWhitespace(message), StringUtil.collapseWhitespace(p.toString()));
        }
    }

    @Test
    public void testWithComplexPluralMessage() throws Exception {
        String message = "{itemCount, plural, one {There is one item.} other {There are # items. {users, plural, one {One user} other {# users}}}}";
        try (MessageFormatParser p = new MessageFormatParser()) {
            p.parse(message);
            assertEquals(StringUtil.collapseWhitespace(message), StringUtil.collapseWhitespace(p.toString()));
        }
    }

    @Test
    public void testWithNestedGenderAndPluralMessage() throws Exception {
        String message = "{userGender, select, male {{user} has} female {{user} has} other {{user} has}} invited {guestCount, plural, one {one guest} other {{guestCount, plural, one {# guest} other {# guests}}}} to the {eventType, select, party {party} conference {conference} other {event}}.";
        try (MessageFormatParser p = new MessageFormatParser()) {
            p.parse(message);
            assertEquals(StringUtil.collapseWhitespace(message), StringUtil.collapseWhitespace(p.toString()));
        }
    }

    @Test
    public void testWithMixedGenderAndPluralMessage() throws Exception {
        String message = "For {userGender, select, male {him} female {her} other {them}}, there {itemCount, plural, one {is one item} other {are # items}} in the {location, select, home {home} office {office} other {location}}.";
        try (MessageFormatParser p = new MessageFormatParser()) {
            p.parse(message);
            assertEquals(StringUtil.collapseWhitespace(message), StringUtil.collapseWhitespace(p.toString()));
        }
    }

    @Test
    public void testWithEmbeddedPluralMessage() throws Exception {
        String message = "{0, plural, one {You have {1, plural, one {# apple} other {# apples}}} other {You and # others have {1, plural, one {# apple} other {# apples}}}}";
        try (MessageFormatParser p = new MessageFormatParser()) {
            p.parse(message);
            assertEquals(StringUtil.collapseWhitespace(message), StringUtil.collapseWhitespace(p.toString()));
        }
    }

    @Test
    public void testSelectMessage() throws Exception {
        String message = "{color, select, red {The color is red.} blue {The color is blue.} green {The color is green.} other {The color is unknown.}}";
        try (MessageFormatParser p = new MessageFormatParser(message)) {
            assertEquals(StringUtil.collapseWhitespace(message), StringUtil.collapseWhitespace(p.toString()));
        }
    }

    @Test
    public void testSelectOrdinalMessage() throws Exception {
        String message = "{position, selectordinal, one {You are in the 1st place.} two {You are in the 2nd place.} few {You are in the 3rd place.} other {You are in the #th place.}}";
        try (MessageFormatParser p = new MessageFormatParser(message)) {
            assertEquals(StringUtil.collapseWhitespace(message), StringUtil.collapseWhitespace(p.toString()));
        }
    }

    @Test()
    // shows all significant whitespace is preserved
    public void testFormattedMessage() throws Exception {
        String message = "{gender_of_host, select,\n" +
                "    female {   \n  {num_guests, plural, offset:1\n" +
                "        =0 {{host} \r\n \r \n        does not give a party.}\n" +
                "        =1 {{host} invites {guest} to her party.}\n" +
                "        =2 {{host} invites {guest} and one other person to her party.}\n" +
                "        other {{host} invites {guest} and  other people to her party.}}}\n" +
                "    male {\n" +
                "    {num_guests, plural, offset:1\n" +
                "        =0 {{host} does not give a party.}\n" +
                "        =1 {{host} invites {guest} to his party.}\n" +
                "        =2 {{host} invites {guest} and one other person to his party.}\n" +
                "        other {{host} invites {guest} and  other people to his party.}}}\n" +
                "    other {\n" +
                "    {num_guests, plural, offset:1\n" +
                "        =0 {{host} does not give a party.}\n" +
                "        =1 {{host} invites {guest} to their party.}\n" +
                "        =2 {{host} invites {guest} and one other person to their party.}\n" +
                "        other {{host} invites {guest} and  other people to their party.}}}}";
        try (MessageFormatParser p = new MessageFormatParser(message)) {
            assertEquals(message, p.toString());
        }
    }

    @Test(expected = OkapiBadFilterInputException.class)
    public void testChoiceMessage() throws Exception {
        String message = "The value {count, choice,-1#is negative|0#is zero or fraction|1#is one|1.0<is 1+|2#is two|2<is more than 2}, ok?";
        try (MessageFormatParser p = new MessageFormatParser(message)) {
            assertEquals(StringUtil.collapseWhitespace(message), StringUtil.collapseWhitespace(p.toString()));
        }
    }

    @Test(expected = OkapiBadFilterInputException.class)
    public void testCountChoice() throws Exception {
        String message = "The value {count, choice, 0 #is none |1 #is one |1 <is more than one}";
        try (MessageFormatParser p = new MessageFormatParser()) {
            p.parse(message);
            assertEquals(StringUtil.collapseWhitespace(message), StringUtil.collapseWhitespace(p.toString()));
        }
    }
}
