import java.util.stream.Stream;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.EventType;
import net.sf.okapi.common.pipeline.BasePipelineStep;
import net.sf.okapi.common.pipeline.IPipelineStep;
import net.sf.okapi.common.resource.Ending;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.StartGroup;
import net.sf.okapi.common.resource.TextUnit;

public class OneToManyStep extends BasePipelineStep implements IPipelineStep {
	public static final String ONE_TO_MANY_PREFIX = "n2mMap";

	@Override
	public String getName() {
		return this.getClass().getSimpleName();
	}

	@Override
	public String getDescription() {
		return "Some very simple step that splits one TextUnit into several";
	}

	int groupId = 1;

	@Override
	public Stream<Event> handleStream(Event event) {
		if (event.isTextUnit()) {
			ITextUnit tu = event.getTextUnit();
			String text = tu.getSource().getCodedText();
			String[] parts = text.split("\\|\\|\\|");
			if (parts.length > 1) {
				Event [] tmpList = new Event[parts.length + 2];
				String gid = "n2mMap" + groupId++;
				StartGroup sg = new StartGroup();
				sg.setId(gid);
				tmpList[0] = new Event(EventType.START_GROUP, sg);
				for (int i = 0; i < parts.length; i++) {
					TextUnit newTu = new TextUnit(gid + "::" + i, "<<" + parts[i] + ">>");
					tmpList[i + 1] = new Event(EventType.TEXT_UNIT, newTu);
				}
				Ending eg = new Ending(gid);
				tmpList[tmpList.length - 1] = new Event(EventType.END_GROUP, eg);
				return Stream.of(tmpList);
			}
		}
		return Stream.of(event);
	}
}
