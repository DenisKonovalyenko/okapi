import java.util.StringJoiner;
import java.util.stream.Stream;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.EventType;
import net.sf.okapi.common.pipeline.BasePipelineStep;
import net.sf.okapi.common.pipeline.IPipelineStep;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.TextUnit;

public class ManyToOneStep extends BasePipelineStep implements IPipelineStep {
	public static final String ONE_TO_MANY_PREFIX = "n2mMap";

	@Override
	public String getName() {
		return this.getClass().getSimpleName();
	}

	@Override
	public String getDescription() {
		return "Some very simple step that merges several TextUnits into one";
	}

	StringJoiner tuText = null;
	@Override
	public Stream<Event> handleStream(Event event) {
		switch (event.getEventType()) {
			case START_GROUP:
				if (event.getStartGroup().getId().startsWith(ONE_TO_MANY_PREFIX)) {
					tuText = new StringJoiner("|||", "[[", "]]");
					return Stream.of();
				}
				break;
			case TEXT_UNIT:
				if (tuText != null) {
					ITextUnit tu = event.getTextUnit();
					String text = tu.getSource().getCodedText();
					tuText.add(text);
					return Stream.of();
				}
				break;
			case END_GROUP:
				if (tuText != null) {
					TextUnit tu = new TextUnit("tuid", tuText.toString());
					tuText = null;
					return Stream.of(new Event(EventType.TEXT_UNIT, tu));
				}
				break;
		}
		return Stream.of(event);
	}
}
