/*===========================================================================
  Copyright (C) 2018 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.function.Consumer;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.common.resource.TextUnitUtil;
import net.sf.okapi.filters.properties.PropertiesFilter;

public class Main {

	private static final LocaleId TARGET_LOCALE = LocaleId.FRENCH;
	private static final LocaleId SOURCE_LOCALE = LocaleId.ENGLISH;
	private static final String ENCODING = "UTF-8";
	private static final String INPUT_FILE = "test.properties";
	private static final String OUT_ROOT = new File(Main.class.getResource(INPUT_FILE).getPath()).getParent();

	public static void main (String[] args) {

		try (InputStream fis = Main.class.getResourceAsStream(INPUT_FILE);
				RawDocument doc = new RawDocument(fis, ENCODING, SOURCE_LOCALE, TARGET_LOCALE);
				PropertiesFilter filter = new PropertiesFilter()) {

			filter.open(doc, true);

			OneToManyStep o2m = new OneToManyStep();
			ManyToOneStep m2o = new ManyToOneStep();

			Consumer<Event> printEvent = (e) -> {
				String text;
				if (e.isTextUnit()) {
					text = TextUnitUtil.toText(e.getTextUnit().getSource().getFirstContent());
				} else {
					text = e.getResource().toString();
				}
				System.out.printf("%s // %s = \"%s\"%n", e.toString(), e.getResource().getId(), text);
			};

			filter.stream()
					.peek((e) -> System.out.println(""))
					.peek(printEvent)
					.flatMap(o2m::handleStream)
					.peek((e) -> System.out.print("\033[96m"))
					.peek(printEvent)
					.flatMap(m2o::handleStream)
					.peek((e) -> System.out.print("\033[93m"))
					.peek(printEvent)
					.forEach((e) -> System.out.print("\033[m")); // reset color
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
