/*===========================================================================
  Copyright (C) 2023 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/
package net.sf.okapi.lib.segmentation;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class RussianSRXTest {
	private static OkapiSegmenter segmenter;

	@BeforeClass
	public static void init() {
		segmenter = new OkapiSegmenter("ru");
	}

	@Test
	public void testOkapiSegmentTest() {
		segment();
	}

	private void segment() {		
		test("Отток капитала из России составил 7 млрд. долларов, сообщил министр финансов Алексей Кудрин.");
		test("Журнал издаётся с 1967 г., пользуется большой популярностью в мире.");
		test("С 2007 г. периодичность выхода газеты – 120 раз в год.");
		test("Редакция журнала находится в здании по адресу: г. Москва, 110000, улица Мира, д. 1.");
		test("Все эти вопросы заставляют нас искать ответы в нашей истории 60-80-х гг. прошлого столетия.");
		test("Более 300 тыс. документов и справочников.");
		test("Скидки до 50000 руб. на автомобили.");
		test("Изготовление визиток любыми тиражами (от 20 шт. до 10 тысяч) в минимальные сроки (от 20 минут).");
		test("Объем составляет 5 куб.м.");
		test("Маленькая девочка бежала и кричала:", " «Не видали маму?».");
		test("Сегодня 27.10.14");
		test("«Я приду поздно»,  — сказал Андрей.");
		//testSplit("«К чему ты готовишься? – спросила мама. – Завтра ведь выходной».");
		test("Он сказал:", " «Я очень устал», и сразу же замолчал.");
		test("Мне стало как-то ужасно грустно в это мгновение; однако что-то похожее на смех зашевелилось в душе моей.");
		test("Шухов как был в ватных брюках, не снятых на ночь (повыше левого колена их тоже был пришит затасканный, погрязневший лоскут, и на нем выведен черной, уже поблекшей краской номер Щ-854), надел телогрейку…");
		test("Слово «дом» является синонимом жилища");
		test("В Санкт-Петербург на гастроли приехал театр «Современник»");
		test("1°C соответствует 33.8°F.");
		test("Едем на скорости 90 км/ч в сторону пгт. Брагиновка, о котором мы так много слышали по ТВ!");
		test("Д-р ветеринарных наук А. И. Семенов и пр. выступали на этом семинаре.");
		//testSplit("Маленькая девочка бежала и кричала: «Не видали маму?»");
		test("Напоминаю Вам, что 25.10 день рождения у Маши К., нужно будет купить ей подарок.");
		test("Кв. 234 находится на 4 этаже.");
		test("Нужно купить 1)рыбу 2)соль.");
	}

	private void test(final String... sentences) {
		SrxSplitCompare.compare(sentences, segmenter);
	}
}
