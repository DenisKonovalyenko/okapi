/*===========================================================================
  Copyright (C) 2023 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/
package net.sf.okapi.lib.segmentation;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class UkrainianSRXTest {
    private static OkapiSegmenter segmenter;

    @BeforeClass
    public static void init() {
        segmenter = new OkapiSegmenter("uk");
    }

    @Test
    public void testOkapiSegmentTest() {
        segment();
    }

    private void segment() {
        test("Це просте речення.");
        test("Вони приїхали в Париж.", " Але там їм геть не сподобалося.");
        test("Панк-рок — напрям у рок-музиці, що виник у середині 1970-х рр. у США і Великобританії.");
        test("Разом із втечами, вже у XV ст. почастішали збройні виступи селян.");
        test("На початок 1994 р. державний борг України становив 4,8 млрд. дол.");
        test("Київ, вул. Сагайдачного, буд. 43, кв. 4.");
        test("Наша зустріч з А. Марчуком і Г. В. Тріскою відбулася в грудні минулого року.");
        test("Наша зустріч з А.Марчуком і М.В.Хвилею відбулася в грудні минулого року.");
        test("Комендант преподобний С. Мокітімі");
        test("Комендант преподобний С.С.Мокітімі 1.");
        test("Комендант преподобний С.С. Мокітімі 2.");
        test("Склад: акад. Вернадський, проф. Харченко, доц. Семеняк.");
        test("Опергрупа приїхала в с. Лісове.");
        test("300 р. до н. е.");
        test("Пролісок (рос. пролесок) — маленька квітка.");
        //testSplit("Квітка Цісик (англ. Kvitka Cisyk також Kacey Cisyk від ініціалів К.С.); 4 квітня 1953р., Квінз, Нью-Йорк — 29 березня 1998 р., Мангеттен, Нью-Йорк) — американська співачка українського походження.");
        test("До Інституту ім. Глієра під'їжджає чорне авто.");
        test("До табору «Артек».");
    }

    private void test(final String... sentences) {
        SrxSplitCompare.compare(sentences, segmenter);
    }
}
