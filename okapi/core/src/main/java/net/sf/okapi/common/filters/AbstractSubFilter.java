package net.sf.okapi.common.filters;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.encoder.IEncoder;
import net.sf.okapi.common.resource.*;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public abstract class AbstractSubFilter extends AbstractFilter implements ISubFilter {
    private String parentType;
    private StartSubfilter startSubFilter;
    private String parentName;
    private String parentId;
    private int sectionIndex;
    private IEncoder parentEncoder;
    private EndSubfilter endSubFilter;
    private SubFilterEventConverter converter;
    private ITextUnit parentTexUnit;

    public AbstractSubFilter() {
        super();
    }

    protected Event convertEvent(Event event) {
        if (getConverter() != null) {
            return getConverter().convertEvent(event);
        }
        return event;
    }

    @Override
    public List<Event> getEvents(RawDocument input) {
        List<Event> events = new LinkedList<>();
        try {
            open(input);
            while (hasNext()) {
                events.add(next());
            }
        } finally {
            close();
        }
        return Collections.unmodifiableList(events);
    }

    @Override
    public List<Event> getEvents(ITextUnit tu, LocaleId sourceLocale, LocaleId targetLocale) {
        List<Event> events = new LinkedList<>();
        // NOTE: we assume that the source content is unsegmented
        // not sure the subfilter should be applied to segments, just know that this will collapse them here.
        try (RawDocument doc = new RawDocument(tu.getSource().getUnSegmentedContentCopy(), sourceLocale, targetLocale)) {
            open(doc);
            setParentUnit(tu);
            while (hasNext()) {
                Event e = next();
                if (e.isTextUnit()) {
                    ITextUnit stu = e.getTextUnit();
                    // TODO: initial implementation. We need a merge option for standard annotations
                    //  like net.sf.okapi.filters.xliff2.model.XLIFF2NotesAnnotation
                    //  For now we just copy everything from the parent TU
                    IWithProperties.copy(tu, stu);
                    IWithAnnotations.copy(tu, stu);
                }
                events.add(e);
            }
        } finally {
            close();
        }
        return Collections.unmodifiableList(events);
    }

    private void setParentUnit(ITextUnit tu) {
        if (tu != null) {
            setParentId(tu.getId());
            setParentName(tu.getName());
            setParentType(tu.getType());
            this.parentTexUnit = tu;
        }
    }

    /**
     * @return
     */
    @Override
    public StartSubfilter getStartSubfilter() {
        return startSubFilter;
    }

    /**
     * @param startSubfilter
     */
    @Override
    public void setStartSubfilter(StartSubfilter startSubfilter) {
        this.startSubFilter = startSubfilter;
        // parentTexUnit could be null, but that's ok
        this.startSubFilter.setParentTextUnit(parentTexUnit);
    }

    /**
     * @return
     */
    @Override
    public String getParentName() {
        return parentName;
    }

    /**
     * @param parentName
     */
    @Override
    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    /**
     * @return
     */
    @Override
    public int getSectionIndex() {
        return sectionIndex;
    }

    /**
     * @param sectionIndex
     */
    @Override
    public void setSectionIndex(int sectionIndex) {
        this.sectionIndex = sectionIndex;
    }

    @Override
    public String getParentType() {
        return parentType;
    }

    /**
     * @param parentType
     */
    @Override
    public void setParentType(String parentType) {
        this.parentType = parentType;
    }

    /**
     * @param parentEncoder
     */
    @Override
    public void setParentEncoder(IEncoder parentEncoder) {
        this.parentEncoder = parentEncoder;
    }

    /**
     * @param endSubfilter
     */
    @Override
    public void setEndSubfilter(EndSubfilter endSubfilter) {
        this.endSubFilter = endSubfilter;
    }


    @Override
    public void setConverter(SubFilterEventConverter converter) {
        this.converter = converter;
    }

    @Override
    public SubFilterEventConverter getConverter() {
        return converter;
    }

    @Override
    public String getParentId() {
        return parentId;
    }

    @Override
    public void setParentId(String parentId) {
        this.parentId = parentId;
    }
}
